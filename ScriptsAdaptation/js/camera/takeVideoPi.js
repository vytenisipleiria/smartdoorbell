"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PiCamera = require("pi-camera");
var myCamera = new PiCamera({
    mode: 'video',
    output: './pics/video.h264',
    width: 1920,
    height: 1080,
    timeout: 5000,
    nopreview: true,
});
myCamera.record().then(function (result) {
    // Your video was captured 
})
    .catch(function (error) {
    // Handle your error 
});
