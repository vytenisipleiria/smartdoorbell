"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request = require("request");
var datetime = require("node-datetime");
// request('http://www.google.com', function (error, response, body) {
//   console.log('error:', error); // Print the error if one occurred
//   console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
//   console.log('body:', body); // Print the HTML for the Google homepage.
// });
// --------------------------------------------
// configurações do serviço Web (alterar para URL correta)
// UC-iniciais da UC em minusculas, R-reginme da UC em minusculas (d ou n), Y-numero do grupo (1,2,3,...)
var paramUrl = 'http://www.iot.dei.estg.ipleiria.pt/tsng-d-g5/api/lab02_api_post_switch.php';
var paramAuth = 'group5';
var paramKey = 'switch';
var paramGPIO = '1'; // ***** alterar pelo "valor" do switch  *****
var paramNow = datetime.create(new Date()).format('Y-m-d H:M:S');
// let paramPOST = { data: { 'auth':paramAuth, 'key':paramKey, 'value':paramGPIO, 'date':paramNow } }
var paramPOST = { 'auth': paramAuth, 'key': paramKey, 'value': paramGPIO, 'date': paramNow };
console.log("Sending date: " + paramNow);
// enviar dados para o serviço Web (formato: POST)
console.log("a enviar para o serviço Web...");
// let r = request.post(paramUrl, data=paramPOST)
// let r = request.post(paramUrl, paramPOST)
// let r = request.post(paramUrl, paramPOST, function(err,httpResponse,body){ 
var r = request.post({
    url: paramUrl,
    form: paramPOST
}, function (err, httpResponse, body) {
    if (err) {
        return console.error('upload failed:', err);
    }
    console.log('Upload successful!  Server responded with:', body);
    // console.log("httpResponse:" + JSON.stringify(httpResponse));
});
// // mostrar resultados
// console.log(r)
// console.log(r.text)
// console.log("Fim do programa.")
