var gpio = require('rpi-gpio');
import sleep = require( 'system-sleep' )

let blue: number = 31
let green: number = 33
let red: number = 35

let power: number = 1
let gpioSwitch: number = 29

let oldValue = undefined
gpio.on('change', function(channel, value) {
   console.log('Channel ' + channel + ' value is now ' + value);

    if ( value != oldValue ) {
        
        oldValue = value
        
        if ( channel == gpioSwitch ) {
            if ( value ) {
                gpio.setup(red, gpio.DIR_OUT, () => {
                    gpio.write(red, false, function(err) {
                        if (err) throw err;
                        console.log('Written to pin ' + red);
        
                        gpio.setup(green, gpio.DIR_OUT, () => {
                            gpio.write(green, true, function(err) {
                                if (err) throw err;
                                console.log('Written false to pin ' + green);
                            });
                        })
                        
                    });
                });      
            } else {
                gpio.setup(green, gpio.DIR_OUT, () => {
                    gpio.write(green, false, function(err) {
                        if (err) throw err;
                        console.log('Written to pin ' + green);
        
                        gpio.setup(red, gpio.DIR_OUT, () => {
                            gpio.write(red, true, function(err) {
                                if (err) throw err;
                                console.log('Written false to pin ' + red);
                            });
                        })
                        
                    });
                });   
            }
        }
    }
});

process.on('SIGINT', function() {
    console.log("Caught interrupt signal");

    gpio.setup(green, gpio.DIR_OUT, () => {
        gpio.write(green, false, function(err) {
            if (err) throw err;
            console.log('Written to pin ' + green);

            gpio.setup(red, gpio.DIR_OUT, () => {
                gpio.write(red, false, function(err) {
                    if (err) throw err;
                    console.log('Written false to pin ' + red);
                    
                    process.exit();
                });
            })
            
        });
    });   
    
});

try {
    console.log("CTRL+C para terminar")
    gpio.setup(gpioSwitch, gpio.DIR_IN, gpio.EDGE_BOTH, () => {
        
    })
} catch (error) {
    
} finally {
    
}




/*
print("CTRL+C para terminar")
	while True:
	
		tempInput = GPIO.input(inA)
		
		if tempInput != tempValue:
			GPIO.output(outA, tempInput)
			print(tempInput, end="", flush=True) #sem \n e com flush
			tempValue = tempInput
		#sleep(0.05)
*/