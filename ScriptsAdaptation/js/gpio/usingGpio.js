"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var gpio = require('rpi-gpio');
var sleep = require("system-sleep");
var blue = 31;
var green = 33;
var red = 35;
var writeToPin = function (number, callback) {
    gpio.setup(number, gpio.DIR_OUT, function () {
        gpio.write(number, true, function (err) {
            if (err)
                throw err;
            console.log('Written true to pin ' + number);
            sleep(1000);
            gpio.write(number, false, function (err) {
                if (err)
                    throw err;
                console.log('Written false to pin ' + number);
            });
        });
    });
};
var writeToPins = function () {
    gpio.setup(blue, gpio.DIR_OUT, function () {
        gpio.write(blue, true, function (err) {
            if (err)
                throw err;
            console.log('Written true to pin ' + blue);
            sleep(1000);
            gpio.write(blue, false, function (err) {
                if (err)
                    throw err;
                console.log('Written false to pin ' + blue);
                gpio.setup(green, gpio.DIR_OUT, function () {
                    gpio.write(green, true, function (err) {
                        if (err)
                            throw err;
                        console.log('Written true to pin ' + green);
                        sleep(1000);
                        gpio.write(green, false, function (err) {
                            if (err)
                                throw err;
                            console.log('Written false to pin ' + green);
                            gpio.setup(red, gpio.DIR_OUT, function () {
                                gpio.write(red, true, function (err) {
                                    if (err)
                                        throw err;
                                    console.log('Written true to pin ' + red);
                                    sleep(1000);
                                    gpio.write(red, false, function (err) {
                                        if (err)
                                            throw err;
                                        console.log('Written false to pin ' + red);
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};
try {
    writeToPins();
    // writeToPin(blue, writeToPin)
}
catch (error) {
}
finally {
}
// gpio.setup(6, gpio.DIR_IN, readInput);
// function readInput() {
//    gpio.read(6, function(err, value) {
//        console.log('The value is ' + value);
//    });
// }
// gpio.on('change', function(channel, value) {
//    console.log('Channel ' + channel + ' value is now ' + value);
// });
// // gpio.setup(19, gpio.DIR_IN, gpio.EDGE_BOTH);
// gpio.setup(19, gpio.DIR_OUT, write);
// function write() {
//    gpio.write(19, true, function(err) {
//        if (err) throw err;
//        console.log('Written to pin');
//    });
// }
// try {
//     console.log("CTRL+C para terminar")
//     gpio.open(19, "output", function(err) {		// Open pin 16 for output 
//         if(err) throw err;
//         for (let t of [1,2,3,4,5,6,7,8,9,10]) {
//             if ( t % 2 == 0 ) {
//                 gpio.write(19, 1, function(err, value) {			// Set pin 16 high (1) 
//                     console.log(value);	// The current state of the pin 
//                 });
//             } else {
//                 gpio.write(19, 0, function(err, value) {			// Set pin 16 high (0) 
//                     console.log(value);	// The current state of the pin 
//                 });
//             }
//             sleep(2000)   
//         }
//     });
// } catch (error) {
// } finally {
//     gpio.close(19);						// Close pin 16 
// }
// import gpio = require("pi-gpio");
// import sleep = require("system-sleep")
// gpio.open(13, "output", function(err) {		// Open pin 16 for output 
//     gpio.write(13, 1, function() {			// Set pin 16 high (1) 
//         gpio.close(13);						// Close pin 16 
//     });
// });
// gpio.open(19, "output", function(err) {		// Open pin 16 for output 
//     gpio.write(19, 1, function() {			// Set pin 16 high (1) 
//         gpio.close(19);						// Close pin 16 
//     });
// });
// console.log( gpio )
// gpio.open(15, "input", function(err) {		// Open pin 15 for input 
//     gpio.read(15, function(err, value) {			// Set pin 15 high (1) 
//         if(err) throw err;
//         console.log(value);	// The current state of the pin 
//         gpio.close(15);						// Close pin 15 
//     });
// });
// gpio.open(16, "output", function(err) {		// Open pin 16 for output 
//     gpio.write(16, 1, function(err, value) {			// Set pin 16 high (1) 
//         if(err) throw err;
//         console.log(value);	// The current state of the pin 
//         gpio.close(16);						// Close pin 16 
//     });
// });
// try {
//     console.log("CTRL+C para terminar")
//     gpio.open(19, "output", function(err) {		// Open pin 16 for output 
//         if(err) throw err;
//         for (let t of [1,2,3,4,5,6,7,8,9,10]) {
//             if ( t % 2 == 0 ) {
//                 gpio.write(19, 1, function(err, value) {			// Set pin 16 high (1) 
//                     console.log(value);	// The current state of the pin 
//                 });
//             } else {
//                 gpio.write(19, 0, function(err, value) {			// Set pin 16 high (0) 
//                     console.log(value);	// The current state of the pin 
//                 });
//             }
//             sleep(2000)   
//         }
//     });
// } catch (error) {
// } finally {
//     gpio.close(19);						// Close pin 16 
// }
// try:  
// // print("CTRL+C para terminar")
// // while True:
// //     GPIO.output(outA, 1)
// //     print("GPIO", outA, ": ON")
// //     sleep(1.0)
// //     GPIO.output(outA, 0)
// //     print("GPIO %d : %s" % (outA, "OFF"))   #com formatacao
// //     sleep(1.0)
// // except KeyboardInterrupt:
// // print("\nPrograma terminado pelo utilizador.")		
// // except:
// // print("\nErro!!!")
// finally:
// print("A fazer 'reset' ao GPIO...", end="")
// GPIO.cleanup()
// print("ok.")
// print("Fim do programa.") 
