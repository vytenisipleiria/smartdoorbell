/*
 * SmartDoorBell
 * A Raspberry Pi app for smart door bell solution
 *
 * Original code for KittyCam By Tomomi Imura (@girlie_mac)
 */

'use strict'

const config = require('./config');
const fs = require('fs');
const child_process = require('child_process');

require('events').EventEmitter.prototype._maxListeners = 20;

// Johnny-Five for RPi
const raspi = require('raspi-io');
const five = require('johnny-five');
const board = new five.Board({io: new raspi()});

let i = 0;
let oldValue = undefined
let gpioSwitch = 29
const gpio = require('rpi-gpio');


board.on('ready', () => {
  console.log('board is ready');
  gpio.on('change', function(channel, value) {
    console.log('Channel ' + channel + ' value is now ' + value);
 
    if ( value != oldValue ) {
         
      oldValue = value
         
        if ( channel == gpioSwitch ) {
          console.log('switch position changed');
          
          // Run raspistill command to take a photo with the camera module
          let filename = 'photo/image_'+i+'.jpg';
          let args = ['-w', '320', '-h', '240', '-o', filename, '-t', '1'];
          let spawn = child_process.spawn('raspistill', args);
      
          //Photo has been taken
      
          spawn.on('exit', (code) => {
                //   console.log('A photo is saved as '+filename+ ' with exit code, ' + code);
          })
        }
      }
  });
});


function deletePhoto(imgPath) {
  fs.unlink(imgPath, (err) => {
    if (err) {
       return console.error(err);
    }
    console.log(imgPath + ' is deleted.');
  });
}


// PubNub to publish the data
// to make a separated web/mobile interface can subscribe the data to stream the photos in realtime.

const channel = 'kittyCam';

//TODO needs to be investigated if this will be used
const pubnub = require('pubnub').init({
  subscribe_key: config.pubnub.subscribe_key,
  publish_key: config.pubnub.publish_key
});

function publish(url, timestamp) {
  pubnub.publish({
    channel: channel,
    message: {image: url, timestamp: timestamp},
    callback: (m) => {console.log(m);},
    error: (err) => {console.log(err);}
  });
}

// Cloudinary to store the photos

const cloudinary = require('cloudinary');

cloudinary.config({
  cloud_name: config.cloudinary.cloud_name,
  api_key: config.cloudinary.api_key,
  api_secret: config.cloudinary.api_secret
});

function uploadToCloudinary(base64Img, timestamp) {
  cloudinary.uploader.upload(base64Img, (result) => {
    console.log(result);
    publish(result.url, timestamp); // Comment this out if you don't use PubNub
  });
}

gpio.setup(gpioSwitch, gpio.DIR_IN, gpio.EDGE_BOTH)

// Ctrl-C
process.on('SIGINT', () => {
  process.exit();
});
