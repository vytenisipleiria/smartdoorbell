var gpio = require('rpi-gpio');
var request = require('request');

// import sleep = require( 'system-sleep' )
import NodeWebcam = require( "node-webcam" );

let power: number = 1
let gpioSwitch: number = 29

//Default options 
let opts = {

   //Picture related 
   width: 1280,
   height: 720,
   quality: 100,

   //Delay to take shot 
   delay: 0,

   //Save shots in memory 
   saveShots: true,

   // [jpeg, png] support varies 
   // Webcam.OutputTypes 
   output: "jpeg",

   //Which camera to use 
   //Use Webcam.list() for results 
   //false for default device 
   device: false,

   // [location, buffer, base64] 
   // Webcam.CallbackReturnTypes 
   callbackReturn: "location",

   //Logging 
   verbose: false

};

//Creates webcam instance 
let webcam = NodeWebcam.create( opts );

let picture_name: String = "test_picture"


//-----
var dotenv = require('dotenv');
dotenv.load();

var fs = require('fs');
var cloudinary = require('cloudinary').v2;


var uploads = {};

// set your env variable CLOUDINARY_URL or set the following configuration
cloudinary.config({
    cloud_name: 'dyi8u4qt7',
    api_key: '981161382723683',
    api_secret: 'CKOUBz0kKcgnaCZv9gFSiurW_uY'
});

//-----

// Stream upload
let upload_stream = cloudinary.uploader.upload_stream({ tags: picture_name }, function(err, image) {
    waitForAllUploads(picture_name, err, image);
    if (err) { console.warn(err); }
    return image.public_id;
});
// var file_reader = fs.createReadStream('pizza.jpg').pipe(upload_stream);

let waitForAllUploads = (id, err, image) => {
    uploads[id] = image;
    var ids = Object.keys(uploads);
    
    // image info
    // console.log(image)
    console.log(image.public_id)
    console.log(image.url)
    console.log(image.secure_url)
    console.log(image.tags)

    var recipients = ["2171990@my.ipleiria.pt", "2120188@my.ipleiria.pt", "2171962@my.ipleiria.pt"]

    var paramUrl = 'https://smart-door-bell.scm.azurewebsites.net/api/triggeredwebjobs/sendEmailNotification/run?arguments=';
    var options = {
        method: "post",
        headers: {
          "Authorization": "Basic JHNtYXJ0LWRvb3ItYmVsbDoxR1dGMGRZZWVXaHV3SnJuN0Zvc3ppcjZEOGxyc0x6REJGZURIbDhYSDZYdmZsTXI3dFc0RmduZWQzdWg="
        },
        contentType: "multipart/form-data"
    };

    var paramUrlWithArguments = paramUrl + " " + image.url + " " + recipients.join(" ")

    console.log("Sending emails: ");
    console.log("a enviar para o serviço Web...");
    var r = request.post({
        url: paramUrl,
        form: options
    }, function (err, httpResponse, body) {
        if (err) {
            return console.error('EmailJob failed:', err);
        }
        console.log('EmailJob called successfuly!  Server responded with:', body);
    });
    
}
// ------

let oldValue = undefined
gpio.on('change', function(channel, value) {
   console.log('Channel ' + channel + ' value is now ' + value);

    // control structure used because switch tends to set the value more than one time on each tick
    if ( value != oldValue ) {
        
        oldValue = value
        
        // verifies if switch changed value
        if ( channel == gpioSwitch ) {
            // verifies if switch value is true
            if ( value ) {
                
                //Will automatically append location output type 
                webcam.capture( picture_name, ( err, data ) => {
                    if ( err ) {
                        console.error( "Err! " + err)
                    } else {
                        console.log("Image taken. '" + data + "'")

                        // send code goes here
                        var file_reader = fs.createReadStream(picture_name + '.jpg').pipe(upload_stream);
                        
                    }
                });   
            }
        }
    }
})

process.on('SIGINT', function() {
    console.log("Caught interrupt signal");
    process.exit();    
});

try {
    console.log("CTRL+C para terminar")
    gpio.setup(gpioSwitch, gpio.DIR_IN, gpio.EDGE_BOTH, () => {
        
    })
} catch (error) {
    
} finally {
    
}