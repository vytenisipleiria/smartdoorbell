import PiCamera = require('pi-camera');
const myCamera = new PiCamera({
  mode: 'photo',
  output: './pics/capture.jpg',
  width: 640,
  height: 480,
  nopreview: true,
});
 
myCamera.snap().then((result) => {
    // Your picture was captured
    
}).catch((error) => {
    // Handle your error
     
});