SMARTDOORBELL data flows: https://drive.google.com/file/d/0B9BSr3c9WGsVSDRVaU5LQ1g5b28/view?usp=sharing
Example of how emailSend endpoint should be called: https://script.google.com/d/1YmTsSeW69025h34ja2fOSP64rnIht8Ik4uuAkTonYllaKqzcC_PFrlsL/edit?usp=sharing


## Getting Started

Install the packages:
```sh
$ npm install
```

Execute the workflows:
```sh
$ node smartDoorBell/ScriptsAdaptation/js/implementation/switchTakePhotoSend.js
```

Change the switch position