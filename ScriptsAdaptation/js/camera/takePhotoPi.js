"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PiCamera = require("pi-camera");
var myCamera = new PiCamera({
    mode: 'photo',
    output: './pics/capture.jpg',
    width: 640,
    height: 480,
    nopreview: true,
});
myCamera.snap().then(function (result) {
    // Your picture was captured
}).catch(function (error) {
    // Handle your error
});
